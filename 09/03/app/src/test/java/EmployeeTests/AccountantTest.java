package EmployeeTests;
import org.junit.jupiter.api.Test;

import Employees.Accountant;

import static org.junit.jupiter.api.Assertions.*;

class AccountantTest {

    Accountant accountant = new Accountant("Ivan", 001, "Бухгалтер");
    @Test
    void giveMoney() {
        assertEquals(20, accountant.giveMoney(20));
    }

    @Test
    void testEquals() {
        assertEquals(accountant, new Accountant("Ivan", 001, "Бухгалтер"));
        assertNotEquals(accountant, new Accountant("Gleb", 002, "Бухгалтер"));
        assertNotEquals(accountant, new Accountant("Bogdan", 003, "Бухгалтер"));
        assertNotEquals(accountant, new Accountant("Daniel", 004, "Бухгалтер"));
    }

}

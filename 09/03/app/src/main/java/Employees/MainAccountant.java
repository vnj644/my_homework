package Employees;
import java.util.Arrays;

public class MainAccountant extends Employee {
    Accountant[] subordinates;
    public MainAccountant(String name, int salary, String position) {
        super(name, salary, position);
        
    }

    @Override
    public String toString() {
        return "{" +
                "имя='" + name + '\'' +
                ", должность='" + position + '\'' +
                ", зарплата=" + salary +
                '}';
    }

    public String accountant(){  //характерный метод
        return "Я главный бухгалтер";
      }
}

package Employees;
import java.util.Objects;

public class Worker extends Employee {

    public Worker(String name, int salary, String position) {
        super(name, salary, position);
        
    }

    @Override
    public String toString() {
        return "{" +
                "имя='" + name + '\'' +
                ", должность='" + position + '\'' +
                ", зарплата=" + salary +
                '}';
    }

    public String accountant(){  //характерный метод
        return "Я рабочий";
      }

}

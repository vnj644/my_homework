package Employees;
public class Main {
    public static void main(String[] args) {
        Employee[] employees = {
                new Accountant("One", 1, "Бухгалтер"),
                new Engineer("Two", 2, "Инженер"),
                new MainAccountant("Three", 3, "Главный бухгалтер"),
                new Worker("Four", 4, "Рабочий")
        };

        for (int i = 0; i < employees.length; i++) {
            System.out.println(employees[i]);
        }
    }
}

package Triangles;
public class Triangle extends Polygon {

    public Triangle(int sidesLength) {
        super(sidesLength, 3);
        // sidesCount = 3;
    }

    @Override
    public void print() {
        System.out.println(this);
    }

    @Override
    public String toString() {
        return "Треугольник{" +
                "длина сторон=" + sidesLength +
                ", количество сторон=" + sidesCount +
                ", периметр = " + perimeter +
                '}';
    }
}

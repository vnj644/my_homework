package Triangles;
public class Polygon implements Printable {
    protected int sidesLength;
    protected int sidesCount;
    protected int perimeter;

    public Polygon(int sidesLength, int sidesCount) {
        this.sidesLength = sidesLength; //длина сторон
        this.sidesCount = sidesCount;
        this.perimeter = this.sidesLength * this.sidesCount;
    }

    public int Perimeter(){
       return perimeter;
    }

    @Override
    public String toString() {
        return "Многоугольник{" +
                "длина сторон=" + sidesLength +
                ", количество сторон=" + sidesCount +
                ", периметр = " + perimeter +
                '}';
    }

    @Override
    public void print() {
        System.out.println(this);
    }
}

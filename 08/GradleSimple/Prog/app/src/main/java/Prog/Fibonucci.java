package Prog;

public class Fibonucci {
    public static int[] fibonucci(int num){
        int n0 = 1, n1 = 1, n2;
        int[] fib = new int[num];
        fib[0] = 0; fib[1] = n0; fib[2] = n1;
		for(int i = 3; i < num; i++){
			n2 = n0 + n1;
            fib[i] = n2;
			n0 = n1;
			n1 = n2;
		}
        return fib;
    }

}


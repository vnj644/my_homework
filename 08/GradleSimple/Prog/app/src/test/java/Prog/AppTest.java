package Prog;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class AppTest {
    @Test void fibonacci() {
        int num = 20;
		int[] fibo = Fibonucci.fibonucci(num);
        int number = fibo[19];
        int dispersionNum = 4181;

        assertEquals(number, dispersionNum);
    }

    @Test void getFibo() {
        int num = 30;
		int[] fiboRec = FibonucciRec.getFib(num);
        int number = fiboRec[29];
        int dispersionNum = 832040;

        assertEquals(number, dispersionNum);
    }


}


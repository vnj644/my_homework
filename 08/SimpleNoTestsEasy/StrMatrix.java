package Easy;

public class StrMatrix{
    public static void main(String[] args) {
        int[][] matrix = {
            {1, 2, 3, 4},
            {2, 3, 4, 1},
            {3, 4, 1, 2},
            {4, 1, 2, 3}};
        int m = matrix.length;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++){
                System.out.print(matrix[i][j] + " ");
            }
        }
    }
}


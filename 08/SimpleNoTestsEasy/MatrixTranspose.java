package Easy;
public class MatrixTranspose {
    public static void main(String[] args) {
        int[][] matrix = {
            {1, 2, 3, 4},
            {2, 3, 4, 1},
            {3, 4, 1, 2},
            {4, 1, 2, 3}};
        int m = matrix.length;
        for (int i = 0; i < m; i++) {
            for (int j = i + 1; j < m; j++){
                int temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++){
                System.out.print(matrix[i][j]);
            }
            System.out.println("");
        }
    }
}
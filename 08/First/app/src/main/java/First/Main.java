package First;

import java.util.Scanner;

public class Main {

    private static Scanner scanner;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);
        System.out.print("Введите число: ");
        String str = scanner.nextLine();
        int num = 0;

        if (CheckString.isNumber(str)) {
            num = Integer.parseInt (str);
            System.out.println("Число: " + num);
        } else {
            System.out.println(str + " не число");
        }
    }
}

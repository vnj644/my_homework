package First;

public class CheckString {

    public static boolean isNumber(String str) {
        if (str == null || str.isEmpty()) return false;
        if (str.charAt(0) == '+' || str.charAt(0) == '-' || Character.isDigit(str.charAt(0))){
            for (int i = 1; i < str.length(); i++) {
                if (!Character.isDigit(str.charAt(i))) return false;
            }
        return true;
        }
        return false;
    }
}
